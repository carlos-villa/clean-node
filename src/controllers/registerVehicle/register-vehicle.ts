import { MissingFormalParameter } from '../../errors/client-error'
import { HttpRequest, HttpResponse } from '../../interfaces/http-interface'

export class RegisterVehicle {
  handle (httpRequest: HttpRequest): HttpResponse {
    const requiredProps = ['name', 'model', 'year']

    for (const prop of requiredProps) {
      if (!httpRequest.body[prop]) {
        return {
          statusCode: 400,
          body: new MissingFormalParameter(`${prop}`)
        }
      }
    }
  }
}
