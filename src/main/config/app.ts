import * as express from 'express'
import { config as dotenv } from 'dotenv'
import routes from './routes'
import middlewares from './middlewares'

const app = express()
dotenv()
/**
 * INIT MIDDLEWARES
 */
middlewares(app)
/**
 * INIT ROUTES
 */
routes(app)
export default app
