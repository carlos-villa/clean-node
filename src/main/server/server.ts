import app from '../config/app'
import ConnectDB from '../../infra/db/mongodb/mongodb-connect'

app.listen(process.env.port, () => {
  console.log(`Server Port: ${process.env.port} \x1b[32m%s\x1b[0m`, 'online')
  ConnectDB()
})
