import { InvalidArgument } from '../errors/invalid-arg-error'
import { UrlLogin } from './http-protocols'

describe('Protocols Http and Querys', () => {
  it('Url login', () => {
    const parsedURl = UrlLogin.parsedUrl('http://localhost:3000/login')

    expect(parsedURl.href).toEqual('http://localhost:3000/login')
    expect(parsedURl.port).toBe('3000')
  })

  it('Url user', () => {
    const parsedURl = UrlLogin.parsedUrl('http://localhost:3000/user')

    expect(parsedURl.href).toEqual('http://localhost:3000/user')
    expect(parsedURl.port).toBe('3000')
  })

  it('Response Query', () => {
    const parsedURl = UrlLogin.parsedUrl('http://localhost:3000/login?user=user&password=password')
    const expectAuth = {
      user: 'user',
      password: 'password'
    }

    expect(parsedURl.query).toEqual(expectAuth)
  })

  it('User Query', () => {
    const parsedURl = UrlLogin.parsedUrl('http://localhost:3000/user?user=user&password=password&name=name&lastname=lastname')
    const expectUser = {
      user: 'user',
      password: 'password',
      name: 'name',
      lastname: 'lastname'
    }

    expect(parsedURl.query).toEqual(expectUser)
  })

  it('invalid Url', () => {
    function expectError (): void {
      UrlLogin.parsedUrl('')
    }
    expect(expectError).toThrowError(new InvalidArgument(''))
  })
})
