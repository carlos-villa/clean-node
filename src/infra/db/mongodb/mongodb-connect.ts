import * as mongoose from 'mongoose'

export default (): void => {
  mongoose.connect(process.env.mongodb_url, { useNewUrlParser: true, useUnifiedTopology: true, dbName: 'tdd' }, (err) => {
    if (err) {
      console.log(err.message)
    } else {
      console.log('MongoDB Port: 27017 \x1b[32m%s\x1b[0m', 'online')
    }
  })
}
